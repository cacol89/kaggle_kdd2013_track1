################################################################################
################################################################################
#
# A profile-based solution to KDD cup 2013
# author: cacol89
#
# ................
#
# name_match.py
#
# This file contains the logic for performing author name comparisons, it does
# some sort of maximum weight bipartite matching between two bag of words.
#
################################################################################
################################################################################

import re
from unidecode import unidecode

def split_name(name,hy):
    if not hy:
        return [[x for x in name.split(" ") if x!='']]
    else:
        return [[x for x in name.replace("-"," ").split(" ") if x!=''],
                [x for x in name.replace("-","").split(" ") if x!='']]

def match(name1,name2):
    hy_a,hy_b = "-" in name1, "-" in name2
    hy = hy_a^hy_b
    bags1,bags2 = split_name(name1,hy),split_name(name2,hy)
    maxi = 0
    for b1 in bags1:
        for b2 in bags2:
            maxi = max(maxi, strong_match(b1,b2))
    return maxi

def strong_match(bag1,bag2):
    if len(bag1) > len(bag2):
        bag1,bag2 = bag2,bag1

    def comp(a,b):
        la,lb = len(a),len(b)
        if la > lb:
            a,b,la,lb = b,a,lb,la
        if la==lb and a==b:
            return (1.0,False)
        elif a==b[:la]:
            return (1.0,True)
        return (1-1.0*edit_dist(a,b)/max(len(a),len(b)),False)
    
    graph = [[comp(wi,wj) for wj in bag2] for wi in bag1]
    res = max_match(graph,0,[False for x in bag2],True)/max(len(bag1),1)
    return max(res,0.0)

def max_match(graph,x,mask,all_ini):
    if x == len(graph):
        return 0 if not all_ini else -10
    maxi = 0
    for y in xrange(len(mask)):
        if not mask[y]:
            w = graph[x][y][0]
            ai = all_ini and graph[x][y][1]
            mask[y] = True
            maxi = max(maxi,w+max_match(graph,x+1,mask,ai)) 
            mask[y] = False
    return maxi

def edit_dist(a,b):
    n,m = len(a),len(b)
    dp = [[0 for i in xrange(m+1)] for i in xrange(n+1)]
    for i in xrange(n):
        dp[i][m] = n-i
    for j in xrange(m):
        dp[n][j] = m-j
    for i in xrange(n-1,-1,-1):
        for j in xrange(m-1,-1,-1):
            dp[i][j] = min([
                    dp[i+1][j]+1,
                    dp[i][j+1]+1,
                    dp[i+1][j+1]+(0 if a[i]==b[j] else 1),
                ])
    return dp[0][0]
