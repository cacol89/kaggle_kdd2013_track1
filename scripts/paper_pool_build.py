################################################################################
################################################################################
#
# A profile-based solution to KDD cup 2013
# author: cacol89
#
# ................
#
# paper_pool_build.py
#
# This file contains the logic for building an initial paper-pool, based on
# leaving only publications where an author ID appears several times but with
# similar names.
#
################################################################################
################################################################################

import db_dump,name_match,data_io
from collections import defaultdict


def original_pool():
    pool = defaultdict(list)
    for row in data_io.execute_sql_file('original_publication_set.sql'):
        pool[row[0]].append(row[1])
    data_io.dump_paper_pool(pool)
    

def build_init_pool(db):
    print "building init pool..."
    rel_auth = {}
    for row in data_io.execute_sql_file('all_relevant_authors.sql'):
        rel_auth[row[0]] = True

    wipe_cnt,std_cnt = 0,0
    auth_paper_cnt = defaultdict(lambda: defaultdict(int))
    for rows in db.paper_authors.itervalues():
        for row in rows:
            if row[1] in rel_auth:
                std_cnt += 1
                nm = name_match.match(db.authors[row[1]][1],row[2])
                if nm>=0.75:
                    auth_paper_cnt[row[1]][row[0]]+=1
                else:
                    wipe_cnt += 1

    paper_pool = {}
    wcnt = 0
    for aid in rel_auth.iterkeys():
        sig_p = [pid for pid,cnt in auth_paper_cnt[aid].iteritems() if cnt>1]
        if sig_p==[]:
            wcnt += 1
        paper_pool[aid] = sig_p if sig_p!=[] else auth_paper_cnt[aid].keys()

    print wcnt,"non complete,",("%d/%d wiped"%(wipe_cnt,std_cnt))
    data_io.dump_paper_pool(paper_pool)

def get_paper_pool(db,build_init):
    if build_init:
        #build_init_pool(db)
        original_pool()
    return data_io.read_paper_pool()


if __name__=='__main__':
    db = db_dump.KDDDB(papers_db=False)
    main(db)
