################################################################################
################################################################################
#
# A profile-based solution to KDD cup 2013
# author: cacol89
#
# ................
#
# db_dump.py
#
# This file contains the logic for extracting and performing cleaning tasks
# on the provided database.
#
################################################################################
################################################################################

import data_io,sets,numpy,re,nltk
from unidecode import unidecode

class KDDDB:
    BANNED_KEYWORDS = None
    STEMMER = None

    @staticmethod
    def clean_name(s):
        s = unidecode(s.decode('utf-8')).lower()
        s = s.replace("'","")
        s = re.sub("\\([^\\(\\)]*\\)|[^a-z-]"," ",s).strip()
        b = []
        for w in s.split(" "):
            if 1<len(w)<=3 and w==re.sub("a|e|i|o|u","",w):
                for c in w:
                    b.append(c)
            elif w!='':
                b.append(w)
        return " ".join(b)
    
    @staticmethod
    def clean_keywords(s):
        stm = KDDDB.stemmer()
        s = unidecode(s.decode('utf-8').lower())
        s = re.sub("[^a-z]"," ",s).strip()
        kw = []
        for w in s.split(' '):
            if w not in KDDDB.banned_keywords():
                kw.append(stm.stem(w))
        return " ".join(kw)

    @staticmethod
    def banned_keywords():
        if KDDDB.BANNED_KEYWORDS == None:
            bk = {}
            for w in ["keywords","keyword","key","word","words",""]:
                bk[w] = True
            for l in ['english','spanish','german','french']:
                for w in nltk.corpus.stopwords.words(l):
                    bk[unidecode(w.decode('utf-8'))] = True
            KDDDB.BANNED_KEYWORDS = bk
        return KDDDB.BANNED_KEYWORDS
    
    @staticmethod
    def stemmer():
        if KDDDB.STEMMER == None:
            stm = nltk.stem.snowball.EnglishStemmer()
            KDDDB.STEMMER = stm
        return KDDDB.STEMMER

    def __init__(self,author_db=True,papers_db=True):
        print '...loading DB'
        if author_db:
            self.paper_authors = self._fetch_paper_authors()
            self.authors = self._fetch_authors()
        if papers_db:
            self.papers = self._fetch_papers()
        print '...DB loaded'

    def _fetch_papers(self):
        papers = {}
        rows = data_io.execute_sql_file('all_relevant_papers.sql')
        for row in rows:
            cr = list(row)
            if cr[2]>2013 or cr[2]<1800:
                cr[2] = 0
            if cr[3] < 0:
                cr[3] = 0
            if cr[4] < 0:
                cr[4] = 0
            cr[5] = KDDDB.clean_keywords(cr[5])
            papers[cr[0]] = cr

        return papers


    def _fetch_paper_authors(self):
        paper_authors = {}
        rows = data_io.execute_sql_file('all_relevant_paperauthors.sql')
        for row in rows:
            cr = list(row)
            cr[2] = KDDDB.clean_name(cr[2])
            if cr[0] not in paper_authors:
                paper_authors[cr[0]] = []
            paper_authors[cr[0]].append(cr)
        return paper_authors

    
    def _fetch_authors(self):
        authors = {}
        rows = data_io.execute_sql_file('all_relevant_authors.sql')
        for row in rows:
            cr = list(row)
            cr[1] = KDDDB.clean_name(cr[1])
            authors[cr[0]] = cr
        return authors

