################################################################################
################################################################################
#
# A profile-based solution to KDD cup 2013
# author: cacol89
#
# ................
#
# data_io.py
#
# This file contains the logic for performing all IO operations for solving
# the problem. This includes reading and executing SQL files, and parsing the
# settings file.
#
################################################################################
################################################################################

import psycopg2,json,os,pickle

SETTINGS_FILE = 'SETTINGS.json'
SETTINGS = None
DB_CONN = None

def execute_sql_file(file_name,rep = {}):
    sql = open('sql/'+file_name).read().strip()
    for k,v in rep.iteritems():
        sql = sql.replace(k,v)
    return execute_sql(sql)

def execute_sql(sql):
    conn = get_db_conn()
    cursor = conn.cursor()
    cursor.execute(sql)
    return cursor.fetchall()

def get_settings():
    global SETTINGS
    if SETTINGS == None:
        global SETTINGS_FILE
        SETTINGS = {}
        paths = json.loads(open(SETTINGS_FILE).read())
        for key,value in paths.iteritems():
            SETTINGS[key] = os.path.expandvars(value)
    return SETTINGS

def get_db_conn():
    global DB_CONN
    if DB_CONN == None:
        conn_string = get_settings()["postgres_conn_string"]
        DB_CONN = psycopg2.connect(conn_string)
    return DB_CONN

def fetch_table(table_name):
    return execute_sql_file('dump_table.sql',{'##TableName##':table_name})  

def write_solution(author_preds):
    f = open(get_settings()["submission_path"],'w')
    f.write('AuthorId,PaperIds\n')
    for aid,p_lst in author_preds.iteritems():
        p_str = " ".join([str(x) for x in p_lst])
        f.write("%d,%s\n"%(aid,p_str))

def dump_paper_pool(paper_pool):
    f_out = open('paper_pool.csv','w')
    for aid,p_lst in paper_pool.iteritems():
        p_lst_str = " ".join([str(x) for x in p_lst])
        f_out.write("%d,%s\n"%(aid,p_lst_str))
    f_out.close()

def read_paper_pool():
    f_in = open('paper_pool.csv','r')
    auth_p = {}
    for line in f_in:
        sp = line.split(',')
        aid = int(sp[0])
        p_lst = [int(x) for x in sp[1].split(' ')]
        auth_p[aid] = p_lst
    f_in.close()
    return auth_p

def save_model(model):
    out_path = get_settings()["model_path"]
    pickle.dump(model, open(out_path, "w"))

def load_model():
    in_path = get_settings()["model_path"]
    return pickle.load(open(in_path))

