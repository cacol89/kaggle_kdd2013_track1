################################################################################
################################################################################
#
# A profile-based solution to KDD cup 2013
# author: cacol89
#
# ................
#
# train.py
#
# This file contains the logic needed to train the model and save it in a file
#
################################################################################
################################################################################

import data_io, profile, time, paper_pool_build
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn import cross_validation

def get_classifier():
    return RandomForestClassifier(  n_estimators=50,
                                    n_jobs=6,
                                    max_features = 6,
                                    min_samples_split=1,
                                    max_depth=None,
                                    random_state=84293)

def main():

    build_init_paper_pool = True
    cross_validate = False

    print "Building author profiles..."
    auth_prof = profile.AuthorProfile.get_author_profiles(build_init_paper_pool)
   
    print "Preparing the training set..."
    pos_set = data_io.execute_sql_file('cleaned_trainconfirmed.sql')
    neg_set = data_io.execute_sql_file('cleaned_traindeleted.sql')

    train_features = profile.AuthorProfile.get_features(pos_set+neg_set,auth_prof)
    target = [1 for x in xrange(len(pos_set))] + [0 for x in xrange(len(neg_set))]

    classifier = get_classifier()
   
    if cross_validate: 
        print "Cross-validating the classifier..."
        scores = cross_validation.cross_val_score(classifier,train_features,target,cv=5)
        print("Accuracy: %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() / 2))

    print "Training the Classifier..."
    classifier.fit(train_features, target)

    print "Saving the Classifier..."
    data_io.save_model(classifier)

if __name__=="__main__":
    main()
