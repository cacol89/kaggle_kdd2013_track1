################################################################################
################################################################################
#
# A profile-based solution to KDD cup 2013
# author: cacol89
#
# ................
#
# profile.py
#
# This file contains the logic for building author profiles out of a paper pool
# as well as for extracting features for the classifier.
#
################################################################################
################################################################################

import data_io,sets,numpy,db_dump,name_match,paper_pool_build
from collections import defaultdict
from unidecode import unidecode
from scipy.stats import chisquare
from numpy import array

class AuthorProfile:
    DBDUMP = db_dump.KDDDB()
    
    @staticmethod
    def get_author_profiles(build_init_paper_pool):
        auth_p = paper_pool_build.get_paper_pool(AuthorProfile.DBDUMP,build_init_paper_pool) 
        auth_prof = {}
        for aid,p_lst in auth_p.iteritems():
            auth_prof[aid] = AuthorProfile(aid,p_lst)
        return auth_prof

    @staticmethod
    def get_features(paper_auth_lst,auth_prof):
        return [auth_prof[t[0]].build_features(t[1]) for t in paper_auth_lst]

    def __init__(self,aid,p_list):
        self.author_id = aid
        self.paper_list = p_list

        self.author_name = AuthorProfile.DBDUMP.authors[aid][1]
        self.affiliation = AuthorProfile.DBDUMP.authors[aid][2]
        self.publication_cnt = len(p_list)
        self.journal_cnt = defaultdict(int)
        self.conference_cnt = defaultdict(int)
        self.jc_publications = 0
        year_cnt = []
        self.null_year_cnt = 0
        self.not_null_year_cnt = 0
        self.coauth = defaultdict(int)
        self.coauth_aff = defaultdict(int)
        self.keywords = {}

        for pid in p_list:
            if pid in AuthorProfile.DBDUMP.papers:
                p = AuthorProfile.DBDUMP.papers[pid]
                if p[2] > 0:
                    year_cnt.append(p[2])
                    self.not_null_year_cnt += 1
                else:
                    self.null_year_cnt += 1
                self.conference_cnt[p[3]] += 1
                if p[3] > 0:
                    self.jc_publications += 1
                self.journal_cnt[p[4]] += 1
                if p[4] > 0:
                    self.jc_publications += 1

                for w in p[5].split(" "):
                    self.keywords[w] = True

            for ca in AuthorProfile.DBDUMP.paper_authors[pid]:  
                if ca[1] == aid:
                    continue
                self.coauth[ca[1]]+=1
                if ca[3] != '':
                    self.coauth_aff[ca[3]] += 1

        if year_cnt == []:
            year_cnt = [0]
        self.year_mean = numpy.mean(year_cnt)
        self.year_std = numpy.std(year_cnt)
        if self.year_std == 0:
            self.year_std = 1
        
        self.jc_publications = max(1,self.jc_publications)

        self.tot_keywords = len(self.keywords.keys())

    def build_features(self,pid):
        p = [pid,'',0,0,0,'']
        if pid in AuthorProfile.DBDUMP.papers:
            p = AuthorProfile.DBDUMP.papers[pid]
            
        apps,saff,snam,snam_sid = self._coauth_similarity(pid)
        ca_common_pubs, ca_common_affs = self._coauthor_common_pubs(pid) 
        
        kw_cnt = 0
        p_kwds = p[5].split(' ')
        for w in p_kwds:
            if w in self.keywords:
                kw_cnt += 1

        return [
            abs(p[2]-self.year_mean)/self.year_std,
            self.not_null_year_cnt,
            self.null_year_cnt,
            self.journal_cnt[p[4]],
            0 if p[4]>0 else 1,
            self.conference_cnt[p[3]],
            0 if p[3]>0 else 1,
            self.jc_publications,
            self.publication_cnt,
            len(AuthorProfile.DBDUMP.paper_authors[pid]),
            ca_common_pubs,
            ca_common_affs,
            len(self.coauth),
            apps,
            saff,
            snam,
            snam_sid,
            len(self.author_name),
            0 if self.affiliation != '' else 1,
            len(p_kwds),
            self.tot_keywords,
            kw_cnt
        ]

    def _coauthor_common_pubs(self,paper_id):
        pubs,affs = 0,0
        for ca in AuthorProfile.DBDUMP.paper_authors[paper_id]:
            pubs += self.coauth[ca[1]]
            affs += self.coauth_aff[ca[3]]
        return pubs,affs

    def _coauth_similarity(self,pid):
        apps = 0
        saff = 0
        snam = 0.0
        snam_sid = 0
        same = 0
        for ca in AuthorProfile.DBDUMP.paper_authors[pid]:  
            if same<1 and ca[1:] == [self.author_id,self.author_name,self.affiliation]:
                same+=1
                continue
            if ca[1] == self.author_id:
                apps+=1
            if not (snam,snam_sid) == (1.0,1.0):
                mt = name_match.match(self.author_name,ca[2])
                sid = 1 if self.author_id==ca[1] else 0
                snam,snam_sid = max((snam,snam_sid),(mt,sid))
            if ca[3] == self.affiliation:
                saff += 1
        return apps,saff,snam,snam_sid



    


