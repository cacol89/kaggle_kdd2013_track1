################################################################################
################################################################################
#
# A profile-based solution to KDD cup 2013
# author: cacol89
#
# ................
#
# predict.py
#
# This file contains the logic for reading a trained model and using it for
# prediction. It will also generate the solutions to the contest.
#
################################################################################
################################################################################

import data_io, profile, time, paper_pool_build
from sklearn.ensemble import RandomForestClassifier
from collections import defaultdict

def group_predictions(p_set,preds):
    author_preds = defaultdict(list)
    for i in xrange(len(p_set)):
        author_preds[p_set[i][0]].append((preds[i],p_set[i][1]))
    
    author_paper_preds = {}
    for aid,p_lst in author_preds.iteritems():
        p_lst.sort(reverse=True)
        author_paper_preds[aid] = [t[1] for t in p_lst]

    return author_paper_preds
    
def main():

    build_init_paper_pool = False

    print "Building author profiles..."
    auth_prof = profile.AuthorProfile.get_author_profiles(build_init_paper_pool)
    
    print "Preparing the prediction set..."
    pred_set = data_io.fetch_table('ValidPaper')
    pred_features = profile.AuthorProfile.get_features(pred_set,auth_prof)
   
    print "Loading classifier..."
    classifier = data_io.load_model()

    print "Making predictions..."
    predictions = list(classifier.predict_proba(pred_features)[:,1])

    print "Formating & writting the output..."  
    author_paper_predictions = group_predictions(pred_set,predictions)
    data_io.write_solution(author_paper_predictions)
       

if __name__=="__main__":
    main()
