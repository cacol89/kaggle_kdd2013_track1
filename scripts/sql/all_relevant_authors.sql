SELECT *
FROM Author
WHERE id IN (
    SELECT DISTINCT AuthorId FROM ValidPaper
    UNION
    SELECT DISTINCT AuthorId FROM TrainConfirmed
    UNION
    SELECT DISTINCT AuthorId FROM TestPaper
)
