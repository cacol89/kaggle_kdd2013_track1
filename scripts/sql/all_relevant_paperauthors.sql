SELECT *
FROM PaperAuthor
WHERE paperid IN (
    SELECT DISTINCT PaperId
    FROM PaperAuthor
    WHERE AuthorId IN (
        SELECT DISTINCT AuthorId FROM ValidPaper
        UNION
        SELECT DISTINCT AuthorId FROM TrainConfirmed
        UNION
        SELECT DISTINCT AuthorId FROM TestPaper
    )
)
