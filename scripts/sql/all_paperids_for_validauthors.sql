WITH pa_cnt AS (
    SELECT AuthorId, PaperId, count(*) cnt
    FROM PaperAuthor
    WHERE AuthorId IN (
        SELECT DISTINCT AuthorId FROM ValidPaper
        UNION
        SELECT DISTINCT AuthorId FROM TrainConfirmed
        UNION
        SELECT DISTINCT AuthorId FROM TestPaper
    )
    GROUP BY AuthorId, PaperId
),
rep_auth AS (
    SELECT AuthorId, MAX(cnt) mx
    FROM pa_cnt
    GROUP BY AuthorId
)
SELECT AuthorId, PaperId
FROM pa_cnt
WHERE cnt > 1 AND AuthorId IN (
    SELECT AuthorId
    FROM rep_auth
    WHERE mx > 1
)
UNION
SELECT AuthorId, PaperId
FROM pa_cnt
WHERE AuthorId IN (
    SELECT AuthorId
    FROM rep_auth
    WHERE mx = 1
)
