################################################################################
################################################################################
#
# A profile-based solution to KDD cup 2013
# author: cacol89
#
################################################################################
################################################################################

This file contains the instructions for setting up and running my solution
to the KDD cup 2013.

#########################################
# (I) Technical requirements
#########################################

This program was tested in a HP Pavilion dv6 machine with the following
characteristics:
- Intel core i7 processor.
- 8GB of ram.
- 64-bit Ubuntu 12.10 as OS.

The following software is required to execute the program:

- Python (>= 2.7.3)
- PostgreSQL (>= 9.1.9)
- python-sklearn (>= 0.11.0-2)
- python-unidecode (>= 0.04.5)
- python-nltk (>= 2.0.4)
- python-psycopg2 (>= 2.4.5)

#########################################
# (II) Setting up
#########################################

For setting up the model it is necessary to download the KDD cup 2013
database and set it up in the local machine. The data and instructions
about how to install it can be found on the competition's site:

https://www.kaggle.com/c/kdd-cup-2013-author-paper-identification-challenge

After this is done, two tasks must be done before running the model:

1) Set in the environment variable 'DataPath' the path to this project's root
directory, for example:

$> export DataPath=/home/user/author_profile

2) Modify the file $DataPath/scripts/SETTINGS.json, change the field named
'postgres_conn_string' and place the necessary information for connecting to
the database.

#########################################
# (III) Running the program
#########################################

For running the model two steps must be followed:

1) Train the model

For training the model simply execute the train.py file in the scripts directory,
it will train a random forest classifier and dump it to disk to the path indicated
in the SETTINGS.json file as the field "model_path"

$> python train.py

2) Predicting the results

For predicting the results simply execute the predict.py file, it will load a trained model
from the path indicated in the SETTINGS.json file as the field "model_path", make predictions,
and save them in the path indicated in the settings as the field "submission_path".

$> python predict.py
